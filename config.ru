# frozen_string_literal: true

require_relative './app'
dev = ENV['RACK_ENV'] == 'development'

run((dev ? App : App.freeze.app))
