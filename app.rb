# frozen_string_literal: true

require 'dotenv/load'
require 'roda'
require 'sequel'

db_string = StringIO.new

db_string << ENV['BLAG_DB_PROT']
db_string << '://'
db_string << ENV['BLAG_DB_HOST']
db_string << ':'
db_string << ENV['BLAG_DB_PORT']
db_string << '/'
db_string << ENV['BLAG_DB_NAME']
db_string << '?'
db_string << "user=#{ENV['BLAG_DB_USER']}"
db_string << "&password=#{ENV['BLAG_DB_USER']}"

DB = Sequel.connect db_string.string

Sequel::Model.plugin :json_serializer

require './data/models'
require './api/methods'