# frozen_string_literal: true

require_relative './methods/user'
require_relative './methods/token'

class App < Roda
  plugin :json
  plugin :json_parser
  plugin :halt
  plugin :sessions, secret: ENV['BLAG_APP_SECRET']

  route do |r|
    r.on 'user' do
      r.run Methods::User
    end

    r.on 'token' do
      r.run Methods::Token
    end
  end
end
