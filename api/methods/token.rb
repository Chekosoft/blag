# frozen_string_literal: true

require 'securerandom'

module Methods
  class Token < Roda
    route do |r|
      r.is do
        r.get do
          magic_link = r.query_string['magic_link']
          r.halt 400 if magic_link.nil?

          token = Model::Tokens.where(magic_link: magic_link).first
          r.halt 404 if token.nil?
        end

        r.post do
          email = r.params['email']
          r.halt 400 if email.nil?

          user = Models::User.where(email: email).first
          r.halt 200 if user.nil?

          user.add_token(
            Models::Token.create(
              token: SecureRandom.uuid,
              magic_link: SecureRandom.uuid
            )
          )

          r.halt 200
        end
      end
    end
  end
end
