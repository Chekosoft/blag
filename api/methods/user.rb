# frozen_string_literal: true

module Methods
  class User < Roda
    route do |r|
      r.root do
        { atata: 1 }
      end
    end
  end
end
