# frozen_string_literal: true

Sequel.migration do
  up do
    add_column :users, :external_id, :uuid
  end

  down do
    remove_column :users, :external_id
  end
end
