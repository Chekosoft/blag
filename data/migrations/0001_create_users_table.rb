# frozen_string_literal: true

Sequel.migration do
  up do
    create_table :users do
      primary_key :id
      String :email, size: 256, unique: true
      String :alias, size: 128
      String :name, size: 192

      Time :created_at
      Time :updated_at
    end
  end

  down do
    drop_table :users
  end
end