# frozen_string_literal: true
Sequel.migration do
  up do
    create_table :tokens do
      uuid :token
      uuid :magic_link, unique: true
      FalseClass :taken, default: false
      foreign_key :user_id, :users


      DateTime :created_at
      DateTime :updated_at

      primary_key [:token]
    end
  end

  down do
    drop_table :tokens
  end
end