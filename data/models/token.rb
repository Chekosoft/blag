# frozen_string_literal: true

module Models
  class Token < Sequel::Model
    set_primary_key [:token]
    many_to_one :user
  end

  Token.unrestrict_primary_key

  Token.plugin :timestamps
end
