# frozen_string_literal: true

module Models
  class User < Sequel::Model
    one_to_many :tokens
  end

  User.plugin :timestamps
  User.plugin :uuid, field: :external_id
end